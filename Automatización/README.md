## Proyecto: Demozable-Automation-Screenplay

El proyecto de Automatización Demozable se elabora con la finalidad de evaluar los conocimientos en el área de Automatización de Hernan José Malave Montero, el cual se encuentra interesado en ser parte de la compañia TATA CONSULTANCY SERVICE

### Este proyecto cuenta con las siguientes características:

<ul>
<li>Lenguaje de programación: Java
<li>Framework de Automatización: Serenity
<li>Patrón de diseño: Screenplay
<li>Framework de soporte BDD (Behavior development driver): Cucumber
<li>Lenguaje de BDD (Behavior development driver): Gherkin
<li>Constructor de proyecto: Maven
<li>Nota: El proyecto está diseñado para ser ejecutado en máquinas Windows y navegador Chrome.
</ul>


### Estructura del proyecto:

#### Carpeta funcional:

Está carpeta contiene los documentos para resolver la parte funcional de la prueba técnica, dando respuesta a la H.U-1 y H.U-2, en su interior encontraremos el test plan y los test case.  

#### Carpeta Automatización:

Está carpeta contiene los archivos con que se construyo la automatización, dando respuesta a la H.U-2, en su interior encontraremos:

**src/main/java/dataBase:** Este paquete contiene las conexiones a base de datos, así como las Querys(La carpeta está vacia debido a que no se realizaron conexiones a base de datos ni querys)

**src/main/java/iteractions:** Este paquete maneja todas las funciones de iteraciones con elementos web.
<ul>
<li>Click: se encarga de dar click a los elementos web, si bien la librería de serenity trae esta función por defecto, se genera esta clase con la finalidad de establecer los tiempos implicitos y así mejorar el performace de la automatización
<li>Type: se encarga de escribir en elementos de la página web, si bien la librería de serenity trae esta función por defecto, se genera esta clase con la finalidad de establecer los tiempos implicitos y así mejorar el performace de la automatización
<li>WaitElement: se encarga de esperar los elementos a que sean visible en la página Web, lo que evita que coloquemos tiempos muertos en nuestra automatización ya que se considera una mala práctica debido que el rendimiento se ve afectado.
</ul>

**src/main/java/models:** Este paquete maneja toda la data necesaria para ejecutar las pruebas automatizadas (Login, Register, Contact), esta data la compartimos utilizando la propiedad <b>@Shared</b> 

**src/main/java/tasks:** Este paquete maneja las tareas que va a realizar el usuario para cumplir con lo esperado en la prueba automatizada, así cumpliendo con el Patrón de diseño Screenplay
<ul>
<li>AddProduct: esta clase se encarga de agregar un producto al carro de compras
<li>Login: esta clase se encarga de realizar el Login de un usuario 
<li>FillWrongLogin: Esta clase se encarga de llenar de manera incorrecta el formulario de "Log in", es ejecutada en sus diferentes casos alternos.
<li>Register: esta clase es la encargada de realizar el registro exitoso de un nuevo usuario, adicional comprueba el registro exitoso del nuevo usuario realizando Log in.
<li>FillWrongRegister: Esta clase se encarga de llenar de manera incorrecta el formulario de "Sing up", es ejecutada en sus diferentes casos alternos.
<li>RegisterContact esta clase se encarga de registrar los datos de contacto de usuario (Que haya realizado Log in o no) y enviar un mensaje   
</ul>

**src/main/java/userinterface:** En este paquete referenciamos los elementos de las vistas con las que vamos a interactuar, así garantizando la reusabilidad de código.

**src/main/java/questions:** En este paquete contiene las preguntas que realiza el actor sobre el sistema.

**src/main/java/utils:** En este paquete tenemos las clases que seran utilizadas por las iteracciones y tareas con el fin de que seán reutilizadas las veces que seán necesarias

**src/test/java/runner:** Este paquete contiene las clases las cuales ejecutaran nuestra prueba

**src/test/java/stepsDefinitions:** Este paquete contiene nuestros archivos de steps los cuales definen el paso a paso del lado de lógica de programación

**src/test/features:** Este paquete contiene nuestros archivos feature los cuales definen el escenario a evaluar del lado del BDD

**gitignore:** El archivo encargado de bloquear subir archivos no necesarios en nuestro repositorio

**serenity.properties:** Es el archivo que nos ayuda a setear propiedades del Framework de Serenity



### Pasos para obtener el proyecto de automation:

<ul>
<li>Clonar el repositorio en la ubicación deseada, usando el comando: git clone https://gitlab.com/hmalave17/prueba-tata
</ul>

### Pasos para ejecutar el proyecto

Se debe primero realizar los pasos de la sección "Pasos para obtener el proyecto de automation"
<ul>
<li>Abrir el proyecto en el IDE de su preferencia que soporte Java, ejecutar cualquiera de la clase que está en la carpeta runner, la cual está por defecto con el  @RegressionTest para ejecutar todas las pruebas de las diferentes suites
</ul>

### Generación de reporte del framework
Despues de correr en la prueba, debe colocar en la terminal el comando "mvn serenity:aggregate", el cual generará el reporte en la carpeta /target/site/serenity abriendo el archivo index.html

