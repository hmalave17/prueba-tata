package questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.targets.Target;
import util.GetText;
import util.Words;

public class NameUser implements Question <String> {

    private Target element;
    public NameUser(Target element){
        this.element = element;
    }
    @Override
    public String answeredBy(Actor actor) {
        String nameUser = GetText.required(this.element.resolveFor(actor).getText(), Words.SPACE, Words.POSICION);
        return nameUser;
    }
    public static NameUser demoblaze(Target element){
        return new NameUser(element);
    }
}
