package task;

import interactions.Click;
import interactions.Type;
import interactions.WaitElement;
import models.DataLogin;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Shared;
import userInterface.HomePage;
import userInterface.LoginPage;

public class FillWrongLogin implements Task {

    @Shared
    DataLogin dataLogin;
    private int error;
    public FillWrongLogin(int error){
        this.error = error;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if (this.error == 0){
            actor.attemptsTo(Login.user(dataLogin.getUSER_NAME(), dataLogin.getPASSWORD_INCORRECT()));
            BrowseTheWeb.as(actor).getAlert().accept();
        } if (this.error == 1){
            actor.attemptsTo(Click.on(HomePage.BUTTON_LOGIN));
            actor.attemptsTo(Type.on(LoginPage.INPUT_PASSWORD, dataLogin.getPASSWORD()));
            actor.attemptsTo(Click.on(LoginPage.BUTTON_LOGIN));
        } if(this.error == 2){
            actor.attemptsTo(Click.on(HomePage.BUTTON_LOGIN));
            actor.attemptsTo(Type.on(LoginPage.INPUT_USER_NAME, dataLogin.getUSER_NAME()));
            actor.attemptsTo(Click.on(LoginPage.BUTTON_LOGIN));
        }
        actor.attemptsTo(WaitElement.visible(LoginPage.LABEL_LOGIN));
    }
    public static FillWrongLogin user(int error){
        return Tasks.instrumented(FillWrongLogin.class, error);
    }
}
