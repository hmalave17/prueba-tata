package task;

import interactions.Click;
import interactions.Type;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import userInterface.HomePage;
import userInterface.LoginPage;

public class Login implements Task {

    private String username;
    private String password;

    public Login(String username, String password){
        this.username = username;
        this.password = password;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Click.on(HomePage.BUTTON_LOGIN));
        actor.attemptsTo(Type.on(LoginPage.INPUT_USER_NAME, this.username));
        actor.attemptsTo(Type.on(LoginPage.INPUT_PASSWORD, this.password));
        actor.attemptsTo(Click.on(LoginPage.BUTTON_LOGIN));
    }
    public static Login user(String username, String password){
        return Tasks.instrumented(Login.class, username, password);
    }
}
