package task;

import interactions.Click;
import interactions.Type;
import interactions.WaitElement;
import models.DataContact;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.thucydides.core.annotations.Shared;
import userInterface.ContactPage;
import userInterface.HomePage;

public class RegisterContact implements Task {

    @Shared
    DataContact dataContact;

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Click.on(ContactPage.BUTTON_CONTACT));
        actor.attemptsTo(Type.on(ContactPage.INPUT_CONTACT_EMAIL, dataContact.getEMAIL()));
        actor.attemptsTo(Type.on(ContactPage.INPUT_CONTACT_NAME, dataContact.getCONTACT_NAME()));
        actor.attemptsTo(Type.on(ContactPage.INPUT_CONTACT_MESSAGE, dataContact.getMESSAGE()));
        actor.attemptsTo(Click.on(ContactPage.BUTTON_SEND_MESSAGE));
        actor.attemptsTo(WaitElement.visible(HomePage.BUTTON_CONTACT));
    }

    public static RegisterContact form(){
        return Tasks.instrumented(RegisterContact.class);
    }
}
