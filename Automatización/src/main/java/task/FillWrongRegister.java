package task;

import interactions.Click;
import interactions.Type;
import models.DataLogin;
import models.DataRegister;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Shared;
import userInterface.HomePage;
import userInterface.RegisterPage;
import util.NumberRandom;

public class FillWrongRegister implements Task {

    @Shared
    DataRegister dataRegister;

    @Shared
    DataLogin dataLogin;

    private int error;
    public FillWrongRegister(int error){
        this.error = error;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        if(this.error == 1){
            actor.attemptsTo(Click.on(HomePage.BUTTON_REGISTER));
            actor.attemptsTo(Type.on(RegisterPage.INPUT_PASSWORD, dataLogin.getPASSWORD()));
            actor.attemptsTo(Click.on(RegisterPage.BUTTON_SING_UP));
        } if (this.error == 2){
            actor.attemptsTo(Click.on(HomePage.BUTTON_REGISTER));
            dataRegister.setNEW_USER(dataLogin.getUSER_NAME() + NumberRandom.number());
            actor.attemptsTo(Type.on(RegisterPage.INPUT_USER_NAME, dataRegister.getNEW_USER()));
            actor.attemptsTo(Click.on(RegisterPage.BUTTON_SING_UP));
        } if(this.error == 3){
            actor.attemptsTo(Click.on(HomePage.BUTTON_REGISTER));
            actor.attemptsTo(Type.on(RegisterPage.INPUT_USER_NAME, dataLogin.getUSER_NAME()));
            actor.attemptsTo(Type.on(RegisterPage.INPUT_PASSWORD, dataLogin.getPASSWORD()));
            actor.attemptsTo(Click.on(RegisterPage.BUTTON_SING_UP));
            BrowseTheWeb.as(actor).getAlert().accept();
        }
    }
    public static FillWrongRegister user(int error){
        return Tasks.instrumented(FillWrongRegister.class, error);
    }
}
