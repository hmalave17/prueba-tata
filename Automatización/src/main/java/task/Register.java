package task;

import interactions.Click;
import interactions.Type;
import interactions.WaitElement;
import models.DataLogin;
import models.DataRegister;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.thucydides.core.annotations.Shared;
import userInterface.HomePage;
import userInterface.RegisterPage;
import util.NumberRandom;

public class Register implements Task {

    @Shared
    DataLogin dataLogin;

    @Shared
    DataRegister dataRegister;

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Click.on(HomePage.BUTTON_REGISTER));
        dataRegister.setNEW_USER(dataLogin.getUSER_NAME() + NumberRandom.number());
        actor.attemptsTo(Type.on(RegisterPage.INPUT_USER_NAME, dataRegister.getNEW_USER()));
        actor.attemptsTo(Type.on(RegisterPage.INPUT_PASSWORD, dataLogin.getPASSWORD()));
        actor.attemptsTo(Click.on(RegisterPage.BUTTON_SING_UP));
        actor.attemptsTo(WaitElement.visible(HomePage.BUTTON_LOGIN));
        actor.attemptsTo(Login.user(dataRegister.getNEW_USER(), dataLogin.getPASSWORD()));
        actor.attemptsTo(WaitElement.visible(HomePage.LABEL_USER_NAME));

    }
    public static Register user(){
        return Tasks.instrumented(Register.class);
    }
}
