package task;

import interactions.Click;
import interactions.WaitElement;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Scroll;
import userInterface.HomePage;

public class AddProduct implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Scroll.to(HomePage.LABEL_CATEGOTY));
        actor.attemptsTo(Click.on(HomePage.BUTTON_OPTION_LAPTOPS));
        actor.attemptsTo(WaitElement.visible(HomePage.REFERENCE));
        actor.attemptsTo(Scroll.to(HomePage.BUTTON_MACBOOK_PRO));
        actor.attemptsTo(Click.on(HomePage.BUTTON_MACBOOK_PRO));
        actor.attemptsTo(Click.on(HomePage.BUTTON_ADD_CART));
        BrowseTheWeb.as(actor).getAlert().accept();


    }
    public static AddProduct cart(){
        return Tasks.instrumented(AddProduct.class);
    }
}
