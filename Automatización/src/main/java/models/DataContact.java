package models;

public class DataContact {

    private String EMAIL;
    private String CONTACT_NAME;
    private String MESSAGE;

    public DataContact(){
        EMAIL = "test@test.com";
        CONTACT_NAME = "test";
        MESSAGE = "test one";
    }

    public String getEMAIL() {
        return this.EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getCONTACT_NAME() {
        return this.CONTACT_NAME;
    }

    public void setCONTACT_NAME(String CONTACT_NAME) {
        this.CONTACT_NAME = CONTACT_NAME;
    }

    public String getMESSAGE() {
        return this.MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }
}
