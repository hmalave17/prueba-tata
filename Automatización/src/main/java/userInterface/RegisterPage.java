package userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class RegisterPage extends PageObject {

    public static final Target INPUT_USER_NAME = Target.the("Input user name").located(By.xpath("//*[@id=\"sign-username\"]"));
    public static final Target INPUT_PASSWORD = Target.the("Input password").located(By.xpath("//*[@id=\"sign-password\"]"));
    public static final Target BUTTON_SING_UP = Target.the("Button Sing up").located(By.xpath("//*[@id=\"signInModal\"]/div/div/div[3]/button[2]"));
    public static final Target LABEL_SING_UP = Target.the("Label sing up").located(By.xpath("//*[@id=\"signInModalLabel\"]"));

}
