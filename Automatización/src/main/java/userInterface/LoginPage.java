package userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LoginPage extends PageObject {

    public static final Target INPUT_USER_NAME = Target.the("Input user name").located(By.xpath("//*[@id=\"loginusername\"]"));
    public static final Target INPUT_PASSWORD = Target.the("Input password").located(By.xpath("//*[@id=\"loginpassword\"]"));
    public static final Target BUTTON_LOGIN = Target.the("Button Login").located(By.xpath("//*[@id=\"logInModal\"]/div/div/div[3]/button[2]"));
    public static final Target LABEL_LOGIN = Target.the("label login").located(By.xpath("//*[@id=\"logInModalLabel\"]"));
}
