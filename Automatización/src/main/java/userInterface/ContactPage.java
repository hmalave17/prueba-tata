package userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ContactPage extends PageObject {

    public static final Target BUTTON_CONTACT = Target.the("button contact").located(By.xpath("//*[@id=\"navbarExample\"]/ul/li[2]/a"));
    public static final Target INPUT_CONTACT_EMAIL = Target.the("Input contact email").located(By.xpath("//*[@id=\"recipient-email\"]"));
    public static final Target INPUT_CONTACT_NAME = Target.the("Input contact name").located(By.xpath("//*[@id=\"recipient-name\"]"));
    public static final Target INPUT_CONTACT_MESSAGE = Target.the("Input contact message").located(By.xpath("//*[@id=\"message-text\"]"));
    public static final Target BUTTON_SEND_MESSAGE = Target.the("Button send message").located(By.xpath("//*[@id=\"exampleModal\"]/div/div/div[3]/button[2]"));

}
