package userInterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class HomePage extends PageObject {

    public static final Target BUTTON_LOGIN = Target.the("Button Login").located(By.xpath("//*[@id=\"login2\"]"));
    public static final Target LABEL_USER_NAME = Target.the("Label user name").located(By.xpath("//*[@id=\"nameofuser\"]"));
    public static final Target BUTTON_REGISTER = Target.the("Button register").located(By.xpath("//*[@id=\"signin2\"]"));
    public static final Target BUTTON_CONTACT = Target.the("Button contact").located(By.xpath("//*[@id=\"navbarExample\"]/ul/li[2]"));
    public static final Target BUTTON_OPTION_LAPTOPS = Target.the("Button options laptops").located(By.xpath("/html/body/div[5]/div/div[1]/div/a[3]"));
    public static final Target BUTTON_MACBOOK_PRO = Target.the("Button Macbook pro").located(By.xpath("/html/body/div[5]/div/div[2]/div/div[6]/div/div/h4"));
    public static final Target BUTTON_ADD_CART = Target.the("Button add cart").located(By.xpath("//*[@id=\"tbodyid\"]/div[2]/div/a"));
    public static final Target LABEL_CATEGOTY = Target.the("Label Category").located(By.xpath("//*[@id=\"cat\"]"));
    public static final Target REFERENCE = Target.the("Reference").located(By.xpath("//*[@id=\"tbodyid\"]/div[1]/div/div/h4"));
    public static final Target NAME_PRODUCT = Target.the("Name product").located(By.xpath("/html/body/div[5]/div/div[2]/h2"));
}
