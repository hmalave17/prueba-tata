package util;

public class TypeError {

    public static final int PASSWORD_WRONG = 0;
    public static final int WITH_USER_NAME = 1;
    public static final int WITH_PASSWORD = 2;
    public static final int USER_REGISTRED_IN_WEBSITE = 3;
}
