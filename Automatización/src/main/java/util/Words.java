package util;

public class Words {

    public static final String URL_HOME_PAGE = "url_home_page";
    public static final String SING_UP = "Sign up";
    public static final String LOG_IN = "Log in";
    public static final String CONTACT = "Contact";
    public static final String MACBOOK_PRO = "MacBook Pro";
    public static final String SPACE = " ";
    public static final int POSICION = 1;
}
