package stepsDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.DataLogin;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Shared;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;
import questions.NameUser;
import task.FillWrongLogin;
import task.Login;
import userInterface.HomePage;
import userInterface.LoginPage;
import util.TypeError;
import util.Words;

public class LoginSteps {

    @Shared
    DataLogin dataLogin;


    @Managed(driver = "Chrome")
    WebDriver herBrowser;
    Actor user =Actor.named("user");

    @Given("^user open the website$")
    public void user_open_the_website()  {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(Open.url(SystemEnvironmentVariables.createEnvironmentVariables().getProperty(Words.URL_HOME_PAGE)));
    }

    @When("^user make login$")
    public void user_make_login()  {
        user.attemptsTo(Login.user(dataLogin.getUSER_NAME(), dataLogin.getPASSWORD()));
    }

    @Then("^user should see the interface$")
    public void user_should_see_the_interface() {
        user.should(GivenWhenThen.seeThat(NameUser.demoblaze(HomePage.LABEL_USER_NAME), Matchers.is(Matchers.startsWith(dataLogin.getUSER_NAME()))));
    }

    @When("^user enters bad her data$")
    public void user_enters_bad_her_data() {
        user.attemptsTo(FillWrongLogin.user(TypeError.PASSWORD_WRONG));
    }

    @Then("^user can't make a login$")
    public void user_cant_make_a_login() {
        user.attemptsTo(Ensure.that(LoginPage.LABEL_LOGIN.resolveFor(user).getText()).isEqualToIgnoringCase(Words.LOG_IN));
    }

    @When("^user don't enters her username$")
    public void user_dont_enters_her_username() {
        user.attemptsTo(FillWrongLogin.user(TypeError.WITH_USER_NAME));
    }

    @When("^user don't enters her password$")
    public void user_dont_enters_her_password() {
        user.attemptsTo(FillWrongLogin.user(TypeError.WITH_PASSWORD));
    }

}
