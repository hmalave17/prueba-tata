package stepsDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import task.RegisterContact;
import userInterface.HomePage;
import userInterface.RegisterPage;
import util.Words;

public class ContactSteps {


    @Managed(driver = "Chrome")
    WebDriver herBrowser;
    Actor user =Actor.named("user");


    @When("^User register contact Form$")
    public void user_register_contact_form() {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(RegisterContact.form());
    }

    @Then("^the system should register the form$")
    public void the_system_should_register_the_form() {
        user.attemptsTo(Ensure.that(HomePage.BUTTON_CONTACT.resolveFor(user).getText()).isEqualToIgnoringCase(Words.CONTACT));
    }

}
