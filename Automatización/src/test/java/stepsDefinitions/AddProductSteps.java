package stepsDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import task.AddProduct;
import userInterface.HomePage;
import util.Words;

public class AddProductSteps {

    @Managed(driver = "Chrome")
    WebDriver herBrowser;
    Actor user =Actor.named("user");

    @When("^User add a product$")
    public void user_add_a_product() {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(AddProduct.cart());
    }

    @Then("^the system should add this product$")
    public void the_system_should_add_this_product() {
        user.attemptsTo(Ensure.that(HomePage.NAME_PRODUCT.resolveFor(user).getText()).isEqualToIgnoringCase(Words.MACBOOK_PRO));
    }


}
