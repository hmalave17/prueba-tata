package stepsDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import models.DataRegister;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Shared;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;
import questions.NameUser;
import task.FillWrongRegister;
import task.Register;
import userInterface.HomePage;
import userInterface.RegisterPage;
import util.TypeError;
import util.Words;

public class RegisterSteps {

    @Shared
    DataRegister dataRegister;

    @Managed(driver = "Chrome")
    WebDriver herBrowser;
    Actor user =Actor.named("user");

    @When("^user registered in website$")
    public void user_registered_in_website() {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(Register.user());
    }

    @Then("^user should see the interface a new user$")
    public void user_should_see_the_interface_a_new_user() {
        user.should(GivenWhenThen.seeThat(NameUser.demoblaze(HomePage.LABEL_USER_NAME), Matchers.is(Matchers.startsWith(dataRegister.getNEW_USER()))));
    }

    @When("^user registered in website but don't enters her username$")
    public void user_registered_in_website_but_dont_enters_her_username() {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(FillWrongRegister.user(TypeError.WITH_USER_NAME));
    }

    @Then("^user can't make a register$")
    public void user_cant_make_a_register() throws Throwable {
        user.attemptsTo(Ensure.that(RegisterPage.LABEL_SING_UP.resolveFor(user).getText()).isEqualToIgnoringCase(Words.SING_UP));
    }

    @When("^user registered in website but don't enters her password$")
    public void user_registered_in_website_but_dont_enters_her_password() {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(FillWrongRegister.user(TypeError.WITH_PASSWORD));
    }

    @When("^the username is registered in website$")
    public void the_username_is_registered_in_website() {
        user.can(BrowseTheWeb.with(herBrowser));
        user.attemptsTo(FillWrongRegister.user(TypeError.USER_REGISTRED_IN_WEBSITE));
    }
}
