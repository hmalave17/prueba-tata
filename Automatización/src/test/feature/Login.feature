Feature: User make login successful

  Background:
    Given user open the website

  @MakeLoginSuccessfull @RegressionLogin @RegressionTest
  Scenario: make login successful
    When user make login
    Then user should see the interface

  @PasswordIncorrect @RegressionLogin @RegressionTest
  Scenario: Try make login password incorrect
    When user enters bad her data
    Then user can't make a login

  @WithOutUsername @RegressionLogin @RegressionTest
  Scenario: Try make login with out username
    When user don't enters her username
    Then user can't make a login

  @WithOutPassword @RegressionLogin @RegressionTest
  Scenario: Try make login with password
    When user don't enters her password
    Then user can't make a login


