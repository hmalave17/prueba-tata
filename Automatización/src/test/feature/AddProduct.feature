Feature: User add a product

  Background:
    Given user open the website

  @AddProductSuccessful @RegressionAddProduct @RegressionTest
  Scenario: The user add a product in her cart
    When User add a product
    Then the system should add this product