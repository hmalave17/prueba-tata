Feature: register user

  Background:
    Given user open the website

  @registerUser @RegressionRegister @RegressionTest
  Scenario: the user registered in website
    When user registered in website
    Then user should see the interface a new user

  @RegisterNoUserName @RegressionRegister @RegressionTest
  Scenario: the user try to register without username
    When user registered in website but don't enters her username
    Then user can't make a register

  @RegisterNoPassword @RegressionRegister @RegressionTest
  Scenario: the user try to register without password
    When user registered in website but don't enters her password
    Then user can't make a register

  @RegisterUserRegistered @RegressionRegister @RegressionTest
  Scenario: the username is registered in website
    When the username is registered in website
    Then user can't make a register
